First you wanna pull code from this GitLab repository.

**To run the tests via command line:**

* Navigate to project folder
* Install maven (if not yet installed) -> `brew install maven`
* Execute -> `mvn clean test`

**To run the tests via IDE:**

Run `gistsCRUDsuite.xml` file from `src/test/resources/TestSuites`
There is one test file, containing 4 test cases:

**Create new gist:**
* Get the list of IDs of user's gists before creating new one
* Create new gist
* Get the list of IDs of user's gists after creating
* Verify that amount of gists after creating is greater than amount of gists before creating by 1
* Verify that the ID of created gist is in the list of IDs of user's gists after creating

**Read a gist:**
* Create new gist
* Read this gist
* Verify that ID, public flag, owner's login and one of the filenames are equal to parameters used on a step 1

**Edit a gist:**
* Create new gist
* Read this gist
* Verify and save following parameters: description, files amount, public flag, owner's login 
* Update description of a gist and add one more file
* Verify that description has been updated, one more file has been added to a gist
* Verify that public flag and owner's login haven't changed

**Remove a gist:**
* Get the list of IDs of user's gists before creating new one
* Create new gist
* Get the list of IDs of user's gists after creating
* Verify that amount of gists after creating is greater than amount of gists before creating by 1
* Verify that the ID of created gist is in the list of IDs of user's gists after creating
* Remove this new gist
* Get the list of IDs of user's gists after removing
* Verify that amount of gists before creating equals to amount of gists after removing it
* Verify that the ID of created gist is no longer in the list of IDs of user's gists after removing

All test cases end up removing created gists.

**Personalizing test suite**
In order to change GitHub account, you need to change `user` variable in `GistsTests.java` class as well as (Personal API) `token` in `RESTclient.java` class.

The next would be integrating Allure reporter and integrating test suite in CI/CD pipeline.

