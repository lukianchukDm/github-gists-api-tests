package com.github.base;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Api extends RESTclient {

    /**
     * @param user github username
     * @return list of ids of user's gists
     */
    public List<String> getUsersGists(String user) {
        int pageNumber = 1;
        int itemsPerPage = 9999;

        String endpoint = "users/" + user
                + "/gists?page=" + pageNumber
                + "&per_page=" + itemsPerPage;

        return request(Method.GET, endpoint, "", 200, ContentType.JSON).getList("id");
    }

    /**
     * @param description of a gist
     * @param isPublic    visibility type
     * @param files       added to a gist in format [name, content]
     * @return ID of created gist
     */
    public String createGist(String description, boolean isPublic, HashMap<String, String> files) {
        String endpoint = "/gists";
        String body = buildBody(description, isPublic, files);

        return request(Method.POST, endpoint, body, 201, ContentType.JSON).getString("id");
    }

    /**
     * @param id of a gist that should be removed
     */
    public void removeGist(String id) {
        String endpoint = "/gists/" + id;

        request(Method.DELETE, endpoint, "", 204, null);
    }

    /**
     * @param id of a gist
     * @return the jsonPath gist object
     */
    public JsonPath readGist(String id) {
        String endpoint = "/gists/" + id;

        return request(Method.GET, endpoint, "", 200, ContentType.JSON);
    }

    /**
     * @param id of a gist
     * @return the jsonPath gist object
     */
    public JsonPath editGist(String id, String description, boolean isPublic, HashMap<String, String> files) {
        String endpoint = "/gists/" + id;
        String body = buildBody(description, isPublic, files);

        return request(Method.PATCH, endpoint, body, 200, ContentType.JSON);
    }

    /**
     * @return String body
     */
    private String buildBody(String description, boolean isPublic, HashMap<String, String> files) {
        JSONObject json = new JSONObject();

        for (Map.Entry<String, String> file : files.entrySet()) {
            JSONObject content = new JSONObject();

            content.put("content", file.getValue());
            json.put(file.getKey(), content);
        }

        return "{\"description\":\"" + description
                + "\",\"public\":" + isPublic
                + ",\"files\":" + json.toString();
    }
}

