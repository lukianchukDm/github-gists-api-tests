package com.github.base;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RESTclient {
    private String baseURI = "https://api.github.com/";
    private String token = "75693247131870d233352644f54be320b6fde77d";

    private Header header = new Header("Authorization", "Bearer " + token);

    /**
     * @param method HTTP method
     * @param endpoint String
     * @param body String
     * @param expectedStatusCode int
     * @param expectedStatusContentType response ContentType
     * @return jsonPath object
     */
    protected JsonPath request(Method method, String endpoint, String body, int expectedStatusCode, ContentType expectedStatusContentType) {
        RestAssured.baseURI = baseURI;

        Response response = RestAssured.given().header(header).body(body)
                .when().request(method, endpoint);

        verifyResponse(response, expectedStatusCode, expectedStatusContentType);

        return response.then().extract().response().jsonPath();
    }

    /**
     * Verifies response time, status code, and content  type if given
     */
    private void verifyResponse(Response response, int expectedStatusCode, ContentType expectedContentType) {
        responseTimeCheck(response.getTime());

        response.then().assertThat().statusCode(expectedStatusCode);
        if (expectedContentType != null) verifyContentType(response, expectedContentType);
    }

    /**
     * Verifies content type
     */
    private void verifyContentType(Response response, ContentType expectedContentType) {
        response.then().assertThat().contentType(expectedContentType);
    }

    /**
     * Triggers warning when response time is longer than 5 sec
     */
    private void responseTimeCheck(long responseTime) {
        if (responseTime > 5000)
            System.out.println("WARNING!!! Response time is too high! [" + responseTime + " ms]");
    }
}
