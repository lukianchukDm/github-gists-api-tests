package com.github.base;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.lang.reflect.Method;

@Listeners({com.github.base.TestListener.class})
public class BaseTest {

//    protected static RequestSpecification request;
//    protected static Response response;

    protected String testSuiteName;
    protected String testName;
    protected String testMethodName;

    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method, ITestContext context) {

        this.testSuiteName = context.getSuite().getName();
        this.testName = context.getCurrentXmlTest().getName();
        this.testMethodName = method.getName();
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
    }

}

// baseTest - before, after
// test extends baseTest
// api - endpoints
// Api api = new Api()
// api.createNewGist()
// Api extends RESTclient
// RESTclient making calls