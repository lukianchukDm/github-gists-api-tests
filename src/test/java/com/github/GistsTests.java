package com.github;

import com.github.base.Api;
import com.github.base.TestUtilities;
import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GistsTests extends TestUtilities {
    Api api = new Api();
    SoftAssert softAssert = new SoftAssert();

    private String user = "lukianchukdm";

    private String description = "Description [" + getTodaysDate() + "-" + getSystemTime() + "] ";
    private boolean isPublic = true;

    HashMap<String, String> files = new HashMap<>();

    {
        files.put("nameOne.txt", "This is the content of the first file");
        files.put("nameTwo.txt", "This is the content of the second file");
    }

    @Test(priority = 0)
    public void createNewGist() {
        //get list of gists ids before creating new one
        List<String> ids = api.getUsersGists(user);
        int gistsBefore = ids.size();

        //creating new gist
        String id = api.createGist(description, isPublic, files);

        //get list of gists ids after creating new one
        ids = api.getUsersGists(user);
        int gistsAfter = ids.size();

        //asserting results
        softAssert.assertEquals(gistsAfter - gistsBefore, 1);
        softAssert.assertTrue(ids.contains(id));

        //execute assertions and clean up
        softAssert.assertAll();
        cleanUp(id);
    }

    @Test(priority = 1)
    public void readGist() {
        String firstFileName = files.keySet().stream().skip(1).findFirst().get();

        //creating new gist for the test
        String id = api.createGist(description, isPublic, files);
        //reading this new gist
        JsonPath gist = api.readGist(id);

        //getting some values from this new gist
        String verifyID = gist.getString("id");
        Map<String, Map> verifyFiles = gist.getMap("files");
        String verifyFilename = verifyFiles.get(firstFileName).get("filename").toString();

        boolean verifyIsPublic = gist.getBoolean("public");
        Map<String, String> verifyOwner = gist.getMap("owner");
        String verifyUser = verifyOwner.get("login").toString();

        //verifying these values against creating params
        softAssert.assertEquals(verifyID, id);
        softAssert.assertEquals(verifyFilename, firstFileName);
        softAssert.assertEquals(verifyIsPublic, isPublic);
        softAssert.assertEquals(verifyUser, user);

        //execute assertions and clean up
        softAssert.assertAll();
        cleanUp(id);
    }

    @Test(priority = 2)
    public void editGist() {
        //creating new gist for the test
        String id = api.createGist(description, isPublic, files);
        //reading this new gist
        JsonPath gist = api.readGist(id);

        //getting some values before editing
        String descriptionBefore = gist.getString("description");
        Map<String, Map> filesBefore = gist.getMap("files");
        boolean isPublicBefore = gist.getBoolean("public");
        Map<String, String> ownerBefore = gist.getMap("owner");
        String userBefore = ownerBefore.get("login").toString();

        //verifying these values against creating params
        softAssert.assertEquals(descriptionBefore, description);
        softAssert.assertEquals(filesBefore.size(), 2);
        softAssert.assertEquals(isPublicBefore, isPublic);
        softAssert.assertEquals(userBefore, user);

        //setting values that are gonna be updated
        String updatedDescription = description + " UPDATED";
        String newFileName = "nameThree.txt";
        HashMap<String, String> newFiles = new HashMap<>();
        newFiles.put(newFileName, "This is the content of the third file");

        //editing gist
        JsonPath editedGist = api.editGist(id, updatedDescription, isPublic, newFiles);

        //getting some values after editing
        String descriptionAfter = editedGist.getString("description");
        Map<String, Map> filesAfter = editedGist.getMap("files");
        String verifyFilename = filesAfter.get(newFileName).get("filename").toString();

        boolean isPublicAfter = editedGist.getBoolean("public");
        Map<String, String> ownerAfter = editedGist.getMap("owner");
        String userAfter = ownerAfter.get("login").toString();

        //verifying values that have been updated as well as those that have not
        softAssert.assertEquals(descriptionAfter, updatedDescription);
        softAssert.assertEquals(filesAfter.size(), 3);
        softAssert.assertEquals(verifyFilename, newFileName);
        softAssert.assertEquals(isPublicAfter, isPublic);
        softAssert.assertEquals(userAfter, user);

        //execute assertions and clean up
        softAssert.assertAll();
        cleanUp(id);
    }

    @Test(priority = 3)
    public void removeGist() {
        //get list of gists ids before creating new one
        List<String> ids = api.getUsersGists(user);
        int gistsBefore = ids.size();

        //creating new gist for the test
        String id = api.createGist(description, isPublic, files);

        //get list of gists ids after creating new one
        ids = api.getUsersGists(user);
        int gistsAfterCreating = ids.size();

        //verifying creation
        softAssert.assertEquals(gistsAfterCreating - gistsBefore, 1);
        softAssert.assertTrue(ids.contains(id));

        //removing the gist
        api.removeGist(id);

        //get list of gists ids after removing new one
        ids = api.getUsersGists(user);
        int gistsAfterRemoving = ids.size();

        //verifying removing
        softAssert.assertEquals(gistsAfterRemoving, gistsBefore);
        softAssert.assertFalse(ids.contains(id));

        //execute assertions
        softAssert.assertAll();
    }

    /**
     * removing created gist by ID
     */
    private void cleanUp(String id) {
        api.removeGist(id);
    }

}
